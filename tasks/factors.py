"""
 PURPOSE OF PROGRAM: Simple Program that takes a number and prints the factors of that number.

 AUTHOR: Bala Sundaralingam
 DATE:   24/02/2022

"""

def factors(number):
    # ==============
    myList = []     # Create Empty List called myList

    for i in range(2, number):      # For loop increases from third value to avoid 0 and 1, all the way up to number specified.
        if (number % i == 0):       # If remainder is 0, then it's a factor.
            myList.append(i)        # Hence, add it to myList.

    return(myList)                  # Returns the final list.

    # ==============

print(factors(15))      # Should print [3, 5] to the console
print(factors(12))      # Should print [2, 3, 4, 6] to the console
print(factors(13))      # Should print “[]” (an empty list) to the console
