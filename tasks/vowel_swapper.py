"""
 PURPOSE OF PROGRAM: Simple Program that takes a String and replaces vowels with
                     specific characters according to specification.

 AUTHOR: Bala Sundaralingam
 DATE:   24/02/2022

"""

def vowel_swapper(string):

    # ==============
    # Replaces vowels according to characters given in specification.
    vowelSwap = string.replace("a", "4")\
          .replace("A", "4")\
          .replace("e","3")\
          .replace("E","3")\
          .replace("i","!")\
          .replace("I","!")\
          .replace("o","ooo")\
          .replace("O","OOO")\
          .replace("u","|_|").replace("U","|_|")
    return(vowelSwap)
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console