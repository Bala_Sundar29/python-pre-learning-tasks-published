"""
 PURPOSE OF PROGRAM: Simple Program that takes two Integers a & b, and performs
                     operation according to operator +,-,* or /.
 AUTHOR: Bala Sundaralingam
 DATE:   24/02/2022

"""

def calculator(a, b, operator):

    # ==============
    firstValue = a
    secondValue = b
    mathsOperation = operator

    if mathsOperation == "+":                 # Checks if operator is '+'.
        return(firstValue + secondValue)      # Prints Addition of First value and Second Value.
    elif mathsOperation == "-":               # Checks if operator is '-'.
        return(firstValue - secondValue)      # Prints Subtraction of First value and Second Value.
    elif mathsOperation == "*":               # Checks if operator is '*'.
        return(firstValue * secondValue)      # Prints Multiplication of First value and Second Value.
    elif mathsOperation == "/":               # Checks if operator is '/'.
        return(firstValue / secondValue)      # Prints Division of First value and Second Value.
    else:
        return("Incorrect Mathematical Operation")   # If mathsOperation value is different, it prints as specified.
    # ==============

print(calculator(2, 4, "+"))        # Should print 6 to the console
print(calculator(10, 3, "-"))       # Should print 7 to the console
print(calculator(4, 7, "*"))        # Should print 28 to the console
print(calculator(100, 2, "/"))      # Should print 50 to the console
