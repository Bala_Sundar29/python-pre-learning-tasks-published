"""
 PURPOSE OF PROGRAM: Simple Program that takes a string and swaps vowels
                     with special characters according to specification.

 AUTHOR: Bala Sundaralingam
 DATE:   24/02/2022

"""
def vowel_swapper(string):
    # ==============
    changeStringToLower = string.lower()            # Changes string to lower case letters.
    findFirstA = changeStringToLower.find("a")      # Finds First 'a' in lower case string.
    findFirstE = changeStringToLower.find("e")      # Finds First 'e' in lower case string.
    findFirstI = changeStringToLower.find("i")      # Finds First 'i' in lower case string.
    findFirstO = changeStringToLower.find("o")      # Finds First 'o' in lower case string.
    findFirstU = changeStringToLower.find("u")      # Finds First 'u' in lower case string.

    storeDataSecondA = []   # Create Empty List to store 'A' position values.
    storeDataSecondE = []   # Create Empty List to store 'E' position values.
    storeDataSecondI = []   # Create Empty List to store 'I' position values.
    storeDataSecondO = []   # Create Empty List to store 'O' position values.
    storeDataSecondU = []   # Create Empty List to store 'U' position values.

                       # 0     1    2     3      4     5
    replacementList = ["/\\", "3", "!", "000", "ooo", "\/"]

    lowerCaseString = string.lower()                      # Converts string to lowercase.
    originalStringAsList = list(string)                   # Original String as a List.
    lowerCaseStringAsList = list(lowerCaseString)         # Lowercase string as a List.


    if findFirstA != -1:                                                            # Checks if first 'A' is present.
        storeDataSecondA.append(changeStringToLower.find("a", findFirstA+1))        # Location of second a is found and added to List.

        originalStringAsList[storeDataSecondA[0]] = replacementList[0]              # Swaps '/\' to position of second 'a'.
        edited_string = "".join(originalStringAsList)                               # The List is joined as a String.


    if findFirstI != -1:                                                            # Checks if first 'I' is present.
        storeDataSecondI.append(changeStringToLower.find("i", findFirstI+1))        # Location of second i is found and added to List.

        originalStringAsList[storeDataSecondI[0]] = replacementList[2]              # Swaps '!' to position of second 'i'
        edited_string = "".join(originalStringAsList)                               # The List is joined as a string.

    # Needs to find second 'O' and needs to compare to see if lower case 'o' or upper case 'O'
    if findFirstO != -1:                                                            # Checks if first 'O' is present.
        storeDataSecondO.append(changeStringToLower.find("o", findFirstO + 1))      # Location of second 'o' is found and added to List.

        if string[storeDataSecondO[0]] == "O":                                      # Checks if second 'o' is Upper or Lower case.
            originalStringAsList[storeDataSecondO[0]] = replacementList[3]          # Upper Case: Swaps 'OOO' to position of second 'O'.
            edited_string = "".join(originalStringAsList)                           # The List is joined as a string.
        else:
            originalStringAsList[storeDataSecondO[0]] = replacementList[4]          # Lower Case: Swaps 'ooo' to position of second 'o'.
            edited_string = "".join(originalStringAsList)                           # The List is joined as a string.

    if findFirstU != -1:                                                            # Checks if first 'U' is present.
        storeDataSecondU.append(changeStringToLower.find("u", findFirstU + 1))      # Location of second 'u' is found and added to List.

        originalStringAsList[storeDataSecondU[0]] = replacementList[5]              # Swaps '\/' to position of second 'u'.
        edited_string = "".join(originalStringAsList)                               # The List is joined as a string.

    if findFirstE != -1:                                                            # Checks if first 'E' is present.
        storeDataSecondE.append(changeStringToLower.find("e", findFirstE + 1))      # Location of second 'e' is found and added to List.

        x = storeDataSecondE[0]                                                     # Location of second 'e' assigned to variable.
        if x > 0:                                                                   # Checks if second 'e' is present.
            originalStringAsList[storeDataSecondE[0]] = replacementList[1]          # If present, it swaps '3' with 'e'.
            edited_string = "".join(originalStringAsList)
        else:
            edited_string = "".join(originalStringAsList)                           # Else just returns the edited string.

        return(edited_string)








print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
