"""
 PURPOSE OF PROGRAM: Simple Program that returns the factors of a number and
                     specifies if number is a Prime number.
 AUTHOR: Bala Sundaralingam
 DATE:   24/02/2022

"""

def factors(number):
    # ==============
    myList = []
    for i in range(2, number):
        if (number % i == 0):
            myList.append(i)

    if (myList == []):
        print(str(number) + " is a Prime Number.")

    if (myList != []):
        return (myList)

    return ("Program Works Successfully!!!")


    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
