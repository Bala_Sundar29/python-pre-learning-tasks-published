"""
 PURPOSE OF PROGRAM: Simple Program that takes two values a,b and an operator e.g. '+',
                     and performs necessary operation and outputs a binary value.

 AUTHOR: Bala Sundaralingam
 DATE:   24/02/2022

"""


def calculator(a, b, operator):

    # ==============
    firstValue = a
    secondValue = b
    mathsOperation = operator

    #dataBits = [128, 64, 32, 16, 8, 4, 3, 2, 1]     # Personal remainder containing values of data bits

    # int() - constructs a integer number from an integer literal, float literal and string literal.
    # bin() - returns binary value with prefix "0b".
    # replace() - replaces a value by another value e.g. replaces "0b" with "".

    # ADDITION
    if mathsOperation == "+":                                          # Checks if operator is '+'.
        sumOfAddition = firstValue + secondValue                       # ADDS values a & b.
        return(bin(int(sumOfAddition)).replace("0b", ""))              # Prints Addition of First value and Second Value.


    # SUBTRACTION
    elif mathsOperation == "-":                                        # Checks if operator is '-'.
        sumOfSubtraction = firstValue - secondValue                    # SUBTRACTS values a & b.
        return(bin(int(sumOfSubtraction)).replace("0b", ""))           # Prints Subtraction of First value and Second Value.


    # MULTIPLICATION
    elif mathsOperation == "*":                                        # Checks if operator is '*'.
        sumOfMultiplication = firstValue * secondValue                 # MULTIPLIES values a & b.
        return(bin(int(sumOfMultiplication)).replace("0b", ""))        # Prints Multiplication of First value and Second Value.


    # DIVISION
    elif mathsOperation == "/":                                        # Checks if operator is '/'.
        sumOfDivision = firstValue / secondValue                       # DIVIDES values a & b.
        return(bin(int(sumOfDivision)).replace("0b", ""))              # Prints Division of First value and Second Value.


    # INCORRECT OPERATOR
    else:
        return("Incorrect Mathematical Operation")                      # If mathsOperation value is different, it prints as specified.

    # ==============

print(calculator(2, 4, "+"))        # Should print 110 to the console
print(calculator(10, 3, "-"))       # Should print 111 to the console
print(calculator(4, 7, "*"))        # Should output 11100 to the console
print(calculator(100, 2, "/"))      # Should print 110010 to the console
